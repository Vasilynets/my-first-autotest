package com.gmail.jevhenia.vasilynets;

        import org.junit.AfterClass;
        import org.junit.Assert;
        import org.junit.BeforeClass;
        import org.junit.Test;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.chrome.ChromeDriver;

        import java.util.concurrent.TimeUnit;

public class FirstTest {

    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "/opt/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.google.com/intl/uk/gmail/about/#");
    }
    @Test
    public void userLogin() {
        WebElement sign = driver.findElement(By.cssSelector(".gmail-nav__nav-link.gmail-nav__nav-link__sign-in"));
        sign.click();

        WebElement emailField = driver.findElement(By.id("identifierId"));
        emailField.sendKeys("jevhenia.vasilynets");

        WebElement next1 = driver.findElement(By.id("identifierNext"));
        next1.click();

        WebElement passwordField = driver.findElement(By.cssSelector("input[name='password']"));
        passwordField.sendKeys("maggie9911");

        WebElement next2 = driver.findElement(By.id("passwordNext"));
        next2.click();


    }
    @AfterClass
    public static void tearDown() {
        WebElement iconMenu = driver.findElement(By.cssSelector(".gb_ab.gbii"));
        iconMenu.click();

        WebElement exit = driver.findElement(By.id("gb_71"));
        exit.click();
        driver.quit();
    }
}